<table class="js-file-title">
<thead>
<tr>
<th>

INDICE

</th>
</tr>
</thead>
<tbody>
<tr>
<td>

Este es el índice del ejemplo. Navega por todo el contenido desde aquí. Aquí encontrarás el mapa de contenidos. 

</td>
</tr>
<tr>
<td>

[[_TOC_]]

</td>
</tr>
</tbody>
</table>


<br>

# Objetivos

Desarrollar una aplicacion que nos permita realizar el CRUD de una tabla denominada clientes

# Pasos

Veamos los pasos generales para realizar esta aplicacion

## Crear la aplicacion

~~~php
laravel new aplicacion4
~~~

## Instalar dependencias de node

Entramos en la carpeta de la aplicacion. 

Podemos ejecutar cualquiera de estos dos comandos

~~~php
npm install
~~~

~~~php
npm i
~~~

## Configurar idioma de la aplicacion

En el fichero .env coloco la aplicacion en castellano
~~~php
APP_LOCALE=es
APP_FALLBACK_LOCALE=es
APP_FAKER_LOCALE=es_ES
~~~

## Instalar SASS y BOOTSTRAP

Para esta aplicacion voy a utilizar SASS y Bootstrap
~~~php
npm i sass --save-dev
npm i bootstrap --save-dev
~~~

## Modificar CSS y JS para utilizar bootstrap y sass

Como ya hemos visto en clase necesitamos importar los css de bootstrap y el js de bootstrap

## Incorporar vite al proyecto y configurar 

El vite ya esta instalado debo configurar el fichero incorporando el css y el js.
En las webs recordar que tendre que colocar
~~~php
@vite(['resources/css/app.scss'])
@vite(['resources/js/app.js'])
~~~

## Arrancar el servidor de vite

~~~php
npm run dev
~~~

Acordaros que cuando terminemos la aplicacion para compilar los css y js y dejarlos en desarrollo necesito ejecutar
~~~php
npm run build
~~~

## Crear el modelo para la tabla clientes

Voy a crear el modelo para la tabla clientes y ademas voy a generar:

- migraciones : para generar la tabla
- factories : para crear datos aleatorios para la tabla
- seeder : para introducir datos a la tabla
- controlador : para crear todas acciones que gestionan el funcionamiento de las vistas de clientes

~~~php
php artisan make:model ClienteModel -msfc
~~~

## Ir al fichero de migraciones y crear los campos

En el fichero de migraciones creo los campos para la tabla clientes
~~~php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 200)->nullable(true);
            $table->string('email')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('clientes');
    }
};
~~~

## Ejecuto las migraciones

Ahora quiero que se ejecute la migracion para crear la tabla

~~~php
    php artisan migrate
~~~

## Vamos a crear una serie de registros de prueba

Para crear los registros de prueba voy a realizar lo siguiente:

- modificar factories
- modificar seeders
- modificar configuracion del database
- ejecutar los seeders

### Modificar factories

~~~php

namespace Database\Factories;

use App\Models\Cliente;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Cliente>
 */
class ClienteFactory extends Factory
{
    protected $model = Cliente::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombre' => $this->faker->name(),
            'email' => $this->faker->email()
        ];
    }
}

~~~

### Modifico los seeders

~~~php
namespace Database\Seeders;

use App\Models\Cliente;
use Database\Factories\ClienteFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Cliente::factory(50)->create();
    }
}
~~~

### Configuro databaseSeeder

~~~php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            ClienteSeeder::class,
        ]);
    }
}

~~~


### Creo los registros

~~~php
php artisan db:seed
~~~

## Podemos comprobar que los registros han entrado

Para comprobar que tenemos los registros introducidos podemos utilizar un cliente mysql, la extension vscode que hace de cliente mysql o mediante la herramienta tinker de laravel

~~~php
php artisan tinker
use App\Models\Cliente;
Cliente::all();
~~~

## Ahora vamos a gestionar los controladores de la aplicacion

Voy a utilizar dos controladores

- HomeController : para la pagina de inicio
- ClienteController : para las acciones de gestion de clientes


### El controlador Home

Este controlador solamente gestiona la pagina de inicio

~~~php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        return view('home.index');
    }
}

~~~


### El controlador Cliente

Este controlador deberiamos haberle creado como un controlador de recursos como realizaremos en ejemplos posteriores ya que voy a crear las acciones de todos los verbos de HTTP para la gestion de clientes.

De momento esta vacio pero iremos creando las siguientes acciones (metodos):

- index : listado de todos los clientes
- show : mostrar los datos de un cliente
- edit : mostrar el formulario para actualizar los datos de un cliente
- update : actualizar los datos de un cliente
- delete : eliminar un determinado cliente
- create : mostrar el formulario para crear un cliente
- store : almacenar los datos de un cliente que se va a crear nuevo

## Ahora vamos a gestionar las rutas de la aplicacion en el dispatcher

~~~php

use App\Http\Controllers\ClienteController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

// pagina de inicio
Route::controller(HomeController::class)->group(function () {
    Route::get('/', 'index')->name('home.index');
    Route::get('/index', 'index')->name('home.index');
});

// acciones del controlador de clientes

Route::controller(ClienteController::class)->group(function () {
    Route::get('/cliente', 'index')->name('cliente.index');
    Route::get('/cliente/create', 'create')->name('cliente.create');
    Route::get('/cliente/{id}', 'show')->name('cliente.show');
    Route::post('/cliente', 'store')->name('cliente.store');
    Route::get('/cliente/edit/{id}', 'edit')->name('cliente.edit');
    Route::put('/cliente/{cliente}', 'update')->name('cliente.update');
    Route::delete('/cliente/{cliente}', 'destroy')->name('cliente.destroy');
});


~~~

## Vamos a crear un layout general para todas las vistas

Es mejor que creemos un layout general para poder realizar todas las vistas de una forma mas organizada y limpia.

En views creamos una carpeta layouts. Al layout le llamo main (main.blade.php logicamente)

~~~php
<!DOCTYPE html>
<html lang="es_es" class="h-100">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('titulo', 'Alpe')</title>
    @vite(['resources/css/app.scss'])
</head>

<body class="d-flex flex-column h-100">
    <header class="mt-3">
        @section('cabecera')
            @include('_comun.menu')
        @show
    </header>
    <main class="flex-shrink-0">
        <div class="container">
            <div class="grid-view">
                @yield('contenido')
            </div>
        </div>
    </main>
    @include('_comun/pie')

</body>
@vite(['resources/js/app.js'])

</html>
~~~

## Menu de la aplicacion

El menu de la aplicacion lo creo en una subvista que voy a cargar en el layout main.
Creo en views una carpeta denominada _comun y dentro un archivo llamado "menu.blade.php"

~~~php

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home.index') }}">
            Clientes
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link{{ request()->routeIs('home.index') ? ' active' : '' }}" aria-current="page"
                        href="{{ route('home.index') }}">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('cliente.index') ? ' active' : '' }}"
                        href="{{ route('cliente.index') }}">Listar Clientes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('cliente.create') ? ' active' : '' }}"
                        href="{{ route('cliente.create') }}">Insertar Cliente</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
~~~

## Creo el pie

Como el pie le voy a colocar fijo tambien lo realizo como una subvista en _comun denominada pie.blade.php

~~~php
<footer id="footer" class="bg-dark footer mt-auto py-3 text-muted">
    <div class="container">
        <div class="row d-flex align-items-center" style="height: 50px">
            <div class="col-md-6 text-center text-md-start text-white">Ramon Abramo</div>
            <div class="col-md-6 text-center text-md-end text-white"> Aplicacion Web</div>
        </div>
    </div>
</footer>

~~~

## Creo el resto de vistas

Vamos a crear todas las vistas de momento vacias y despues las vamos a rellenar

- creo una carpeta en views denominada cliente y creo las siguientes vistas
    - create 
    - edit
    - index
    - show
- Creo una carpeta llamada home
    - index

## Vamos a crear la vista index de la aplicacion

Voy a crear esta vista para comprobar que todo va correctamente

~~~php
@extends('layouts.main')

@section('titulo', 'Inicio')

@section('cabecera')
    <section class="pt-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">Aplicacion Web</h1>
                <p class="lead text-muted">Podemos gestionar los clientes de una tabla</p>
            </div>
        </div>
    </section>
    @parent
@endsection

@section('contenido')
    <div class="row mt-3 text-center">
        <div>
            <img src="{{ asset('imgs/1.jpg') }}" class="img-fluid rounded col-3 mb-3" alt="Responsive image">
            <p>
                <a href="{{ route('cliente.index') }}" class="btn btn-primary">Abrir</a>
            </p>
        </div>
    </div>
@endsection
~~~

## Voy a crear la accion index del controlador Cliente

En esta accion tenemos que listar los clientes paginados

~~~php
    public function index()
    {
        //tengo que leer todos los clientes y mandarlos a la vista
        //$clientes = Cliente::all(); // array de clientes
        $clientes = Cliente::paginate(6);

        return view('cliente.index', [
            "clientes" => $clientes // le paso a la vista el array de clientes
        ]);
    }
~~~

## Modificamos la vista index de clientes

~~~php
@extends('layouts.main')

@section('titulo', 'Listado')

@section('cabecera')
    <section class="pt-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">Listado de clientes</h1>
                <p class="lead text-muted">Podemos ver todos los clientes</p>
            </div>
        </div>
    </section>
    @parent
@endsection

@section('contenido')
    @if (session('mensaje'))
        <div class="row m-3">
            <div class="alert alert-info">
                {{ session('mensaje') }}
            </div>
        </div>
    @endif
    <div class="row mt-1 row-cols-1 row-cols-lg-3 row-cols-md-2 g-4">
        @foreach ($clientes as $cliente)
            <div class="col">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <h5 class="card-title">
                            {{ $cliente->id }}
                        </h5>
                        <p class="card-text">
                            Nombre: {{ $cliente->nombre }}
                        </p>
                        <p class="card-text">
                            email: {{ $cliente->email }}
                        </p>
                    </div>
                    <div class="card-footer">
                        <div class="vertical-align-bottom d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <a href="{{ route('cliente.show', $cliente->id) }}" class="btn btn-outline-primary">Ver</a>
                                <a href="{{ route('cliente.edit', $cliente->id) }}"
                                    class="btn btn-outline-primary">Editar</a>
                            </div>
                            <form action="{{ route('cliente.destroy', $cliente) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-outline-danger">Eliminar</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row mt-3">
        {{ $clientes->links() }}
    </div>
@endsection
~~~

## Arreglar la paginacion 

Como la paginacion esta preparada para tailwind y estoy utilizando bootstrap se ve mal.

Para arreglarlo modifico AppServiceProvider

~~~php
<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Paginator::useBootstrap(); // añado para soportar bootstrap 5 en paginador
    }
}
~~~

## Creo la accion para mostrar los datos de un cliente

Voy a crear la accion para mostrar los datos de un cliente

~~~php
    public function show($id)
    {
        $cliente = Cliente::find($id);

        return view('cliente.show', [
            "cliente" => $cliente
        ]);
    }
~~~

## Modifico la vista para mostrar los datos de un cliente

Modifico la vista show de clientes

~~~php
@extends('layouts.main')

@section('titulo', 'Mostrar')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3">Ver cliente</h1>
            <p class="col-lg-10 fs-4">
                Estos son los datos del cliente solicitados
            </p>
        </div>
    </section>
    @parent
@endsection

@section('contenido')
    @if (session('mensaje'))
        <div class="row m-2">
            <div class="alert alert-info">
                {{ session('mensaje') }}
            </div>
        </div>
    @endif
    <div class="row mt-3">
        <div class="col">
            <div class="card shadow-xl">
                <div class="card-body">
                    <h5 class="card-title">
                        {{ $cliente->id }}
                    </h5>
                    <p class="card-text">
                        Nombre: {{ $cliente->nombre }}
                    </p>
                    <p class="card-text">
                        email: {{ $cliente->email }}
                    </p>
                    <div class="card-footer">
                        <div class="vertical-align-bottom d-flex justify-content-between align-items-center">
                            <a href="{{ route('cliente.edit', $cliente->id) }}" class="btn btn-primary">Editar</a>
                            <form action="{{ route('cliente.destroy', $cliente) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

~~~


## Creo la accion para editar cliente

Voy a crear en el clienteController la accion edit para mostrar el formulario para editar un cliente

~~~php

    /**
     * cargar el formulario de editar
     */
    public function edit($id){

        // recupero los datos del cliente a editar
        $cliente = Cliente::find($id);
        
        //mostrar el formulario para editar el cliente
        return view('cliente.edit', [
            'cliente' => $cliente
        ]);

    }
~~~


## Modifico la vista para mostrar el formulario de editar cliente

En la carpeta de las views modifico la vista edit para colocar un formulario 

~~~php
@extends('layouts.main')

@section('titulo', 'Editar')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3">Editar cliente</h1>
            <p class="col-lg-10 fs-4">
                Desde este formulario puedes editar los datos de un cliente
            </p>
        </div>
    </section>
    @parent
@endsection

@section('contenido')
    <div class="row mt-3">
        <div class="col-lg-10 mt-2 mx-auto">
            <form action="{{ route('cliente.update', $cliente) }}" method="post" class="p-4 p-md-5 border rounded-3 bg-light">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="nombre" class="form-label">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $cliente->nombre }}">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $cliente->email }}">
                </div>

                <button type="submit" class="btn btn-primary">Actualizar</button>

        </div>
    @endsection
~~~

## Creo la accion para recibir los datos del formulario de edicion del cliente

Voy a crear una accion denominada update que recibe los datos del formulario de edicion

~~~php

    /**
     * cuando en el formulario de editar
     * pulso el boton de actualizar
     */
    public function update(Request $request, Cliente $cliente){
        
        
        // validar los datos
        $request->validate([
            'nombre' => 'required',
            'email' => 'required'
        ]);

        // actualizar el cliente
        $cliente->update($request->all());

        // redireccionar a la vista show
        return redirect()
            ->route('cliente.show', $cliente->id)
            ->with('mensaje', 'El cliente se ha actualizado correctamente');

    }
~~~

## Creo que la accion para insertar un cliente

En el controlador de clientes voy a crear la accion edit para que me muestre el formulario de crear un cliente.

~~~php
    /**
     * cargar el formulario de editar
     */
    public function edit($id){

        // recupero los datos del cliente a editar
        $cliente = Cliente::find($id);
        
        //mostrar el formulario para editar el cliente
        return view('cliente.edit', [
            'cliente' => $cliente
        ]);

    }
~~~

## Creo la vista para mostrar el formulario de insertar cliente

Voy a crear la vista de crear un cliente. La vista la llamo edit

~~~php
@extends('layouts.main')

@section('titulo', 'Editar')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3">Editar cliente</h1>
            <p class="col-lg-10 fs-4">
                Desde este formulario puedes editar los datos de un cliente
            </p>
        </div>
    </section>
    @parent
@endsection

@section('contenido')
    <div class="row mt-3">
        <div class="col-lg-10 mt-2 mx-auto">
            <form action="{{ route('cliente.update', $cliente) }}" method="post" class="p-4 p-md-5 border rounded-3 bg-light">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="nombre" class="form-label">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $cliente->nombre }}">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $cliente->email }}">
                </div>

                <button type="submit" class="btn btn-primary">Actualizar</button>

        </div>
    @endsection
~~~

## Creo la accion para almacenar los datos de un cliente nuevo

Voy a crear una accion llamada store para almacenar los datos de un cliente nuevo

~~~php
    // cuando en el formulario de crear registro
    // pulso el boton de insertar
    // me llega a este metodo
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'email' => 'required'
        ]);

        // asignacion masiva
        $cliente=Cliente::create($request->all());
        // sin asignacion masiva
        // $cliente= new Cliente();
        // $cliente->nombre = $request->input('nombre');
        // $cliente->email = $request->input('email');
        // $cliente->save();

        // redireccionar a la vista show
        return redirect()
            ->route('cliente.show', $cliente->id)
            ->with('mensaje', 'El cliente se ha creado correctamente');

    }
~~~

## Voy a crear la accion delete para eliminar un cliente

Esta accion me permite eliminar un cliente. Se denomina destroy

~~~php
 /**
     * borrar el cliente
     */
    public function destroy(Cliente $cliente){
        //eliminar el cliente
        $cliente->delete();
        //redireccionar a la vista index
        return redirect()
            ->route('cliente.index')
            ->with('mensaje', 'El cliente se ha eliminado correctamente');
    }
}
~~~

## Como llamar a eliminar

Para llamar a esta accion de eliminar tenemos que realizar un formulario para poder enviar los datos por delete

Esto lo hemos realizado en la vista index de clientes y en la vista show de clientes

~~~php
<form action="{{ route('cliente.destroy', $cliente) }}" method="post">
    @csrf
    @method('DELETE')
    <button type="submit" class="btn btn-outline-danger">Eliminar</button>
</form>
~~~


