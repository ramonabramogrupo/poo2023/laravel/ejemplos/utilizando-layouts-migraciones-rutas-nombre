<html lang="es_es" class="h-100">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>My Yii Application</title>
    @vite(['resources/css/app.scss'])
</head>

<body class="d-flex flex-column h-100">
    <header>
        <nav id="w0" class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="/yiiVisualizadores/web/index.php">Ejemplo 5</a>
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#w0-collapse"
                    aria-controls="w0-collapse" aria-expanded="false" aria-label="Toggle navigation"><span
                        class="navbar-toggler-icon"></span></button>
                <div id="w0-collapse" class="collapse navbar-collapse">
                    <ul id="w1" class="navbar-nav nav">
                        <li class="nav-item"><a class="nav-link active"
                                href="/yiiVisualizadores/web/index.php/site/index">Home</a></li>
                        <li class="dropdown nav-item"><a class="dropdown-toggle nav-link" href="#"
                                data-toggle="dropdown">Array</a>
                            <div id="w2" class="dropdown-menu"><a class="dropdown-item"
                                    href="/yiiVisualizadores/web/index.php/site/ejercicio1">Ejercicio1</a>
                                <a class="dropdown-item"
                                    href="/yiiVisualizadores/web/index.php/site/ejercicio2">Ejercicio2</a>
                                <a class="dropdown-item"
                                    href="/yiiVisualizadores/web/index.php/site/ejercicio3">Ejercicio3</a>
                                <a class="dropdown-item"
                                    href="/yiiVisualizadores/web/index.php/site/ejercicio4">Ejercicio4</a>
                                <a class="dropdown-item"
                                    href="/yiiVisualizadores/web/index.php/site/ejercicio5">Ejercicio5</a>
                                <a class="dropdown-item"
                                    href="/yiiVisualizadores/web/index.php/site/ejercicio6">Ejercicio6</a>
                            </div>
                        </li>
                        <li class="dropdown nav-item"><a class="dropdown-toggle nav-link" href="#"
                                data-toggle="dropdown">Modelos Active Record</a>
                            <div id="w3" class="dropdown-menu"><a class="dropdown-item"
                                    href="/yiiVisualizadores/web/index.php/site/ejercicio7">Ejercicio7</a>
                                <a class="dropdown-item"
                                    href="/yiiVisualizadores/web/index.php/site/ejercicio8">Ejercicio8</a>
                            </div>
                        </li>
                        <li class="dropdown nav-item"><a class="dropdown-toggle nav-link" href="#"
                                data-toggle="dropdown">Mostrar un registro</a>
                            <div id="w4" class="dropdown-menu"><a class="dropdown-item"
                                    href="/yiiVisualizadores/web/index.php/site/ejercicio9">Ejercicio9</a>
                                <a class="dropdown-item"
                                    href="/yiiVisualizadores/web/index.php/site/ejercicio10">Ejercicio10</a>
                                <a class="dropdown-item"
                                    href="/yiiVisualizadores/web/index.php/site/ejercicio11">Ejercicio11</a>
                                <a class="dropdown-item"
                                    href="/yiiVisualizadores/web/index.php/site/ejercicio12">Ejercicio12</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <main role="main" class="flex-shrink-0">
        <div class="container">

        </div>
    </main>

    <footer class="footer mt-auto py-3 text-muted bg-d bg-dark">
        <div class="container">
            <div class="row d-flex align-items-center" style="height: 50px">
                <div class="col-md-6 text-center text-md-start text-white">Ramon Abramo</div>
                <div class="col-md-6 text-center text-md-end text-white"> Aplicacion Web</div>
            </div>
        </div>
    </footer>


</body>

</html>
