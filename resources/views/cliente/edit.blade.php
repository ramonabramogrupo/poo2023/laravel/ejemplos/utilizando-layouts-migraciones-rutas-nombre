@extends('layouts.main')

@section('titulo', 'Editar')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3">Editar cliente</h1>
            <p class="col-lg-10 fs-4">
                Desde este formulario puedes editar los datos de un cliente
            </p>
        </div>
    </section>
    @parent
@endsection

@section('contenido')
    <div class="row mt-3">
        <div class="col-lg-10 mt-2 mx-auto">
            <form action="{{ route('cliente.update', $cliente) }}" method="post" class="p-4 p-md-5 border rounded-3 bg-light">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="nombre" class="form-label">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $cliente->nombre }}">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $cliente->email }}">
                </div>

                <button type="submit" class="btn btn-primary">Actualizar</button>

        </div>
    @endsection
