@extends('layouts.main')

@section('titulo', 'Nuevo')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3">Nuevo cliente</h1>
            <p class="col-lg-10 fs-4">
                Desde este formulario puedes crear un nuevo cliente
            </p>
        </div>
    </section>
    @parent
@endsection

@section('contenido')
    <div class="row mt-3">
        <div lass="col-lg-10 mt-3 mx-auto">
            <form action="{{ route('cliente.store') }}" method="post" class="p-4 p-md-5 border rounded-3 bg-light">
                @csrf
                <div class="mb-3 form-floating">
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="nombre">
                    <label for="nombre">Nombre</label>
                </div>
                <div class="mb-3 form-floating">
                    <input type="email" class="form-control" id="email" name="email" placeholder="email">
                    <label for="email">Email</label>
                </div>

                <button type="submit" class="btn btn-primary">Insertar</button>

        </div>

    @endsection
