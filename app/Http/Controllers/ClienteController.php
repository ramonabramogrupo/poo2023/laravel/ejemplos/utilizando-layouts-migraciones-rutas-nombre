<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index()
    {
        //tengo que leer todos los clientes y mandarlos a la vista
        //$clientes = Cliente::all(); // array de clientes
        $clientes = Cliente::paginate(6);

        return view('cliente.index', [
            "clientes" => $clientes // le paso a la vista el array de clientes
        ]);
    }

    public function show($id)
    {
        $cliente = Cliente::find($id);

        return view('cliente.show', [
            "cliente" => $cliente
        ]);
    }

    // nos carga el formulario para crear un nuevo registro
    public function create()
    {
        return view('cliente.create');
    }

    // cuando en el formulario de crear registro
    // pulso el boton de insertar
    // me llega a este metodo
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'email' => 'required'
        ]);

        // asignacion masiva
        $cliente=Cliente::create($request->all());
        // sin asignacion masiva
        // $cliente= new Cliente();
        // $cliente->nombre = $request->input('nombre');
        // $cliente->email = $request->input('email');
        // $cliente->save();

        // redireccionar a la vista show
        return redirect()
            ->route('cliente.show', $cliente->id)
            ->with('mensaje', 'El cliente se ha creado correctamente');

    }

    /**
     * cargar el formulario de editar
     */
    public function edit($id){

        // recupero los datos del cliente a editar
        $cliente = Cliente::find($id);
        
        //mostrar el formulario para editar el cliente
        return view('cliente.edit', [
            'cliente' => $cliente
        ]);

    }

    /**
     * cuando en el formulario de editar
     * pulso el boton de actualizar
     */
    public function update(Request $request, Cliente $cliente){
        
        
        // validar los datos
        $request->validate([
            'nombre' => 'required',
            'email' => 'required'
        ]);

        // actualizar el cliente
        $cliente->update($request->all());

        // redireccionar a la vista show
        return redirect()
            ->route('cliente.show', $cliente->id)
            ->with('mensaje', 'El cliente se ha actualizado correctamente');

    }

    /**
     * borrar el cliente
     */
    public function destroy(Cliente $cliente){
        //eliminar el cliente
        $cliente->delete();
        //redireccionar a la vista index
        return redirect()
            ->route('cliente.index')
            ->with('mensaje', 'El cliente se ha eliminado correctamente');
    }
}
